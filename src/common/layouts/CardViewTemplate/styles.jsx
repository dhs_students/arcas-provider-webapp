// @flow

const formInputAdornments = {
  inputAdornmentContainer: {
    paddingTop: 5
  },
  inputAdornmentLabel: {
    paddingLeft: 6,
    paddingTop: 1
  }
};

const filterSidebar = (theme: any) => ({
  listSidebarWrapper: {
    paddingLeft: 10,
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
  drawerSidebarContainer: {},
  listSidebarContainer: {
    height: "100%",
    padding: 15
  },
  filterLocationContainer: {
    paddingTop: 10,
    paddingBottom: 10
  },
  filterSearchContainer: {
    paddingTop: 10,
    paddingBottom: 10
  },
  filterCategoryContainer: {
    paddingTop: 10,
    paddingBottom: 20
  },
  filterCategoryTitle: {
    paddingBottom: 10
  },
  filterButtonContainer: {
    [theme.breakpoints.up("lg")]: {
      display: "none"
    },
    [theme.breakpoints.down("xs")]: {
      marginTop: 0,
      marginLeft: 0
    }
  },
  filterButton: {
    marginBottom: 10
  }
});

const card = (theme: any) => ({
  card: {
    display: "flex",
    width: "100%"
  },
  cardAvatar: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  cardOrg: {
    paddingTop: 6,
    float: "clear"
  },
  orgContainer: {
    display: "flex",
    flexDirection: "row"
  },
  dateLocationContainer: {
    paddingTop: 15
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "100%"
  },
  content: {
    flex: "1 0 auto",
    width: "100%"
  },
  cover: {
    minWidth: 151,
    maxWidth: 151
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  playIcon: {
    height: 38,
    width: 38
  }
});

const styles = (theme: any) => ({
  ...formInputAdornments,
  ...filterSidebar(theme),
  ...card(theme),
  root: {
    [theme.breakpoints.up("md")]: {
      width: "100%"
    }
  },
  cardContainer: {
    marginBottom: 10
  },
  button: {
    margin: theme.spacing(1)
  },
  fab: {
    margin: theme.spacing(3),
    position: "fixed",
    bottom: theme.spacing(3),
    right: theme.spacing(2),
    zIndex: 2
  },
  table: {
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  search: {
    display: "flex",
    paddingLeft: theme.spacing(1),
    alignItems: "center",
    right: 30,
    top: 120,
    position: "absolute"
  },
  image: {
    width: 84,
    height: 84,
    textAlign: "center"
  },
  img: {
    margin: "auto",
    display: "block",
    width: 84,
    height: 84,
    padding: 10
  },
  cardDescription: {
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },

  cardsContainer: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    [theme.breakpoints.up("md")]: {
      paddingLeft: 50,
      paddingRight: 50
    }
  },
  drawerContent: {
    padding: 20,
    width: 280
  },
  listDescription: {
    paddingTop: 10
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column"
    }
  },
  iconActions: {
    textAlign: "right"
  },
  media: {
    // ⚠️ object-fit is not supported by IE 11.
    objectFit: "cover"
  },
  tabContainer: {
    backgroundColor: "#fff"
  },
  tab: {
    textColorPrimary: "#fff"
  },
  sliderControl: {
    marginTop: 25
  },
  distanceSlider: {
    paddingTop: 95,
    paddingBottom: 15
  },
  pageContent: {
    [theme.breakpoints.down("sm")]: {
      marginLeft: 0,
      marginRight: 0
    },
    paddingBottom: 60
  },
  paginationContainer: {
    paddingLeft: 15,
    paddingTop: 15,
    paddingRight: 15,
    display: "inline-block",
    "& selected a": {
      color: "blue"
    },
    "& ul": {
      display: "inline-block",
      width: "100%",
      listStyleType: "none"
    },
    "& ul li": {
      "&:hover": {
        color: theme.palette.primary.dark
      },
      "& a": {
        padding: 10
      },
      display: "inline-block",
      cursor: "pointer",
      padding: 5
    }
  },
  paginationItem: {
    ...buttonStyles(theme)
  },
  activeLinkClassName: {
    color: theme.palette.primary.dark,
    fontWeight: "bold",
    isplay: "inline-block"
  },
  nextButton: {
    display: "inline-block",
    padding: 10,
    "&:hover": {
      color: theme.palette.primary.dark
    }
  }
});

const buttonStyles = (theme: any) => ({
  paginationPage: {
    display: "inline-block",
    "&:hover": {
      color: theme.palette.primary.dark
    },
    font: theme.typography.font
  }
});

export default styles;
