// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer, inject } from "mobx-react";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Drawer from "@material-ui/core/Drawer";
import { observable, computed, action } from "mobx";
import Pagination from "react-paginate";
import styles from "./styles";

type Props = {
  classes: any,
  sideBarButtonTitle: any,
  sideBarContent: any,
  disableDrawer: any,
  header: any,
  cards: any,
  cardsPerPage: any
};

@inject("AuthStore")
@observer
class CardViewTemplate extends Component<Props> {
  @observable
  sidebarOpen: boolean;

  @observable
  cardsPerPage: number;

  @observable
  currentPage: number;

  constructor(props: Object) {
    super(props);
    this.currentPage = 1;
    this.cardsPerPage = this.props.cardsPerPage ? this.props.cardsPerPage : 12;
    this.sidebarOpen = false;
  }

  toggleDrawer = () => {
    this.sidebarOpen = !this.sidebarOpen;
  };

  @computed
  get hasSidebar(): boolean {
    return this.props.sideBarContent != null;
  }

  renderCardsForPage = (currentPage: number): Array<Object> => {
    const { cards } = this.props;
    if (cards) {
      const page = currentPage === 0 ? 1 : currentPage;
      const offset = this.cardsPerPage;
      const startingIndex = (page - 1) * (offset === 0 ? 1 : offset);
      return cards.slice(startingIndex, startingIndex + this.cardsPerPage);
    }
    return [];
  };

  render() {
    const { classes, disableDrawer, header, sideBarContent } = this.props;
    return (
      <Grid container className={classes.pageContent}>
        <Grid item xs={12} sm={12} md={12} lg={sideBarContent != null ? 9 : 12}>
          <div className={classes.headerContainer}>
            {header != null && (
              <Grid className={classes.listDescription} item xs={12}>
                {header}
              </Grid>
            )}
            {!disableDrawer && (
              <Grid
                className={classes.filterButtonContainer}
                item
                xs={12}
                sm={3}
              >
                <Button
                  onClick={this.toggleDrawer}
                  className={classes.filterButton}
                  color="primary"
                >
                  {this.props.sideBarButtonTitle}
                </Button>
              </Grid>
            )}
          </div>
          <Grid className={classes.cardsContainer} container spacing={2}>
            {this.renderCardsForPage(this.currentPage)}
          </Grid>
          {this.hasSidebar && this.renderPagination()}
        </Grid>
        <Grid className={classes.listSidebarWrapper} item xs={8} sm={3} md={3}>
          <div className={classes.listSidebarContainer}>
            {this.props.sideBarContent}
          </div>
        </Grid>
        <Drawer
          variant="temporary"
          anchor="right"
          open={this.sidebarOpen}
          onClose={this.toggleDrawer}
          className={classes.drawerSidebarContainer}
        >
          <div className={classes.drawerContent}>
            {this.props.sideBarContent}
          </div>
        </Drawer>
        {!this.hasSidebar && this.renderPagination()}
      </Grid>
    );
  }

  @computed
  get pageCount(): number {
    const { cards } = this.props;
    if (cards && cards.length > 0) {
      return Math.ceil(cards.length / this.cardsPerPage);
    }
    return 1;
  }

  @action
  changePage = (pageNumber: Object) => {
    if (pageNumber === 0) {
      this.currentPage = 0;
    } else {
      this.currentPage = pageNumber.selected + 1;
    }
  };

  renderPagination = () => {
    const { classes } = this.props;
    const moreThanOnePage = this.pageCount > 1;
    if (moreThanOnePage) {
      return (
        <Grid
          container
          alignContent="center"
          className={classes.paginationContainer}
        >
          <Pagination
            pageCount={this.pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={2}
            previousLabel="Previous"
            nextLabel="Next"
            onPageChange={this.changePage}
            pageClassName={classes.paginationItem}
            pageLinkClassName={classes.paginationItem}
            nextLinkClassName={classes.paginationItem}
            previousClassName={classes.paginationItem}
            breakClassName={classes.paginationItem}
            activeLinkClassName={classes.activeLinkClassName}
          />
        </Grid>
      );
    }
    return <div />;
  };
}

export default withStyles(styles)(CardViewTemplate);
