// @flow
const styles = (theme: any) => ({
  tabs: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingBottom: 10,
    width: "100%"
  },
  tab: {
    height: 59
  }
});

export default styles;
