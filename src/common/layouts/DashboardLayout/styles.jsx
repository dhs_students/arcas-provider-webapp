// @flow
const drawerWidth = 250;

const styles = (theme: any) => ({
  root: {
    flexGrow: 1,
    zindex: 1,
    overflow: "hidden",
    position: "relative",
    display: "flex",
    width: "100%"
  },
  appBar: {
    position: "absolute",
    marginLeft: drawerWidth,
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  navIconHide: {
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  profilePicture: {
    width: "100%",
    height: "100%",
    backgroundColor: "red"
  },
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up("md")]: {
      position: "relative"
    }
  },
  paper: {
    backgroundColor: "#fff",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column"
  },
  pageHeader: {
    height: "100%",
    display: "flex",
    alignItems: "center"
  },
  pageIcon: {
    marginTop: 45,
    marginLeft: 20,
    height: 45,
    width: 45,
    color: "#fff",
    display: "block"
  },
  pageDescriptionContainer: {
    marginTop: 65,
    height: 120,
    backgroundColor: "#2d323e",
    display: "flex"
  },
  pageTitle: {
    paddingTop: 55,
    paddingLeft: 15,
    color: "#fff"
  },
  pageSubheading: {
    paddingLeft: 30,
    color: "#fff"
  },
  pageContent: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    minHeight: "calc(100vh - 140px)"
  },
  pageContentPadded: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
    marginTop: 10,
    minHeight: "calc(100vh - 140px)"
  },
  appContent: {
    [theme.breakpoints.up("md")]: {
      paddingLeft: drawerWidth
    }
  }
});

export default styles;
