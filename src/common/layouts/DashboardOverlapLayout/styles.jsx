// @flow
const drawerWidth = 250;

const styles = (theme: any) => ({
  root: {
    flexGrow: 1,
    zindex: 1,
    overflow: "hidden",
    position: "relative",
    display: "flex",
    width: "100%"
  },
  pageIcon: {
    marginLeft: 30,
    height: 45,
    width: 45,
    color: "#fff",
    display: "block"
  },
  pageDescriptionContainer: {
    marginTop: 60,
    marginBottom: 20
  },
  pageDescriptionContainerWithAction: {
    marginTop: 60,
    marginBottom: 20
  },
  pageHeaderDetailsContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    height: 120
  },
  pageTitleContainer: {
    display: "flex",
    flexDirection: "row"
  },
  pageHeaderContainer: {
    height: 180,
    backgroundColor: "#2d323e"
  },
  pageHeaderContainerWithAction: {
    height: 180,
    [theme.breakpoints.down("xs")]: {
      height: 236
    },
    backgroundColor: "#2d323e"
  },
  actionButtonContainer: {
    display: "flex",
    flexDirection: "row",
    marginTop: 50,
    [theme.breakpoints.down("xs")]: {
      marginTop: 0,
      marginLeft: 70
    }
  },
  actionButton: {
    marginLeft: 5,
    marginRight: 5
  },
  backButton: {
    marginLeft: 20,
    height: 25,
    color: "#fff",
    textAlign: "left",
    textTransform: "none"
  },
  backIcon: {
    fontSize: 20,
    marginRight: theme.spacing(1)
  },
  pageTitle: {
    paddingTop: 10,
    paddingLeft: 15,
    color: "#fff"
  },
  pageContent: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    minHeight: "calc(100vh - 140px)",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(4),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 0,
      marginRight: 0
    },
    marginTop: -75
  },
  pagePaperContainer: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    minHeight: 400
  },

  appContent: {
    [theme.breakpoints.up("md")]: {
      paddingLeft: drawerWidth
    }
  },
  // tabContainer: {
  //   flexGrow: 1,
  //   borderTopLeftRadius: 8,
  //   borderTopRightRadius: 8,
  //   borderBottomLeftRadius: 8,
  //   borderBottomRightRadius: 8,
  //   backgroundColor: theme.palette.background.paper,
  //   height: 400,
  //   marginLeft: theme.spacing(4),
  //   marginRight: theme.spacing(4),
  //   [theme.breakpoints.down("sm")]: {
  //     marginLeft: 0,
  //     marginRight: 0
  //   }
  // },
  tabs: {
    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(4)
  },
  tab: {
    height: 59
  }
});

export default styles;
