// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import styles from "./styles";

type Props = {
  props: any
};

@observer
class AlertDialogView extends Component<Props> {
  render() {
    const { props } = this.props;
    return (
      <div>
        <Dialog
          open={props.dialogOpen}
          fullWidth
          onClose={props.onNegative}
          maxWidth="xs"
        >
          <DialogTitle id="alert-dialog-title"> {props.title} </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {props.description}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={props.onNegative} color="primary">
              Cancel
            </Button>
            <Button onClick={props.onPositive} color="primary" autoFocus>
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(AlertDialogView);
