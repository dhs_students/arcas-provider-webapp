// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import EditTextDialogView from "./EditTextDialogView";

type Props = {
  defaultValue: any,
  onClose: any,
  onUpdate: any,
  textFieldLabel: any,
  validationRules: any
};

@observer
class EditTextDialog extends Component<Props> {
  @observable
  textFieldValue: string;

  render() {
    return (
      <EditTextDialogView
        controller={this}
        props={this.props}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: any) => {
    const textFieldObj = form.values();
    this.props.onUpdate(textFieldObj.textField);
  };

  @action
  onError = () => {};

  @action
  handleClose = () => {
    this.props.onClose();
  };

  @computed
  get formFields(): Array<Object> {
    const { defaultValue, textFieldLabel, validationRules } = this.props;
    let rules = "required|string";
    if (validationRules !== undefined) {
      rules = validationRules;
    }
    return [
      {
        name: "textField",
        type: "text",
        label: textFieldLabel,
        rules,
        value: defaultValue
      }
    ];
  }
}

export default EditTextDialog;
