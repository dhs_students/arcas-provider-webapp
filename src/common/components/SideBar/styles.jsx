// @flow
const drawerWidth = 250;

const organisationNameContainer = {
  fontWeight: "bold",
  color: "#fff",
  textAlign: "center"
};

const styles = (theme: any) => ({
  root: {
    minHeight: "100vh",
    backgroundColor: "#303030",
    top: 0,
    left: 0,
    position: "fixed"
  },
  groupCaption: {
    paddingLeft: 20,
    paddingBottom: 10,
    paddingTop: 10,
    color: "#c0c0c0"
  },
  orgToolbar: {
    paddingTop: 40,
    margin: 0
  },
  toolbar: {
    ...theme.mixins.toolbar,
    paddingTop: 20,
    backgroundColor: "#3c4252"
  },
  arcasToolbar: {
    paddingLeft: 15
  },
  avatarContainer: {
    height: 40,
    backgroundColor: "#3c4252"
  },
  dot: {
    height: 85,
    width: 90,
    backgroundColor: "#303030",
    "border-radius": "50%",
    display: "inline-block",
    position: "absolute",
    left: 94
  },
  menuItemContainer: {
    paddingTop: 60
  },
  toolbarAvatar: {
    minHeight: 70,
    minWidth: 70,
    backgroundColor: "#fff",
    borderRadius: 40,
    borderColor: "#fff",
    position: "absolute",
    marginTop: 10,
    left: 104
  },
  arcasLogo: {
    width: 20,
    height: 25
  },
  orgLogoContainer: {
    width: "100%",
    textAlign: "center"
  },
  orgLogo: {
    width: "auto",
    maxHeight: 80,
    paddingBottom: 10
  },
  arcasToolbarHeading: {
    color: "#fff",
    fontWeight: 500,
    paddingLeft: 5,
    textAlign: "left"
  },
  organisationName: {
    ...organisationNameContainer
  },
  organisationNameNoLocation: {
    ...organisationNameContainer,
    paddingTop: 20
  },
  organisationCaption: {
    textAlign: "center",
    color: "#fff",
    paddingBottom: 10
  },
  toolbarSubheading: {
    textAlign: "center",
    margin: "auto",
    color: "#c0c0c0"
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#303030",
    [theme.breakpoints.up("md")]: {
      position: "relative"
    },
    position: "absolute",
    top: 0,
    bottom: 0,
    border: 0,
    minHeight: "100vh"
  }
});

export default styles;
