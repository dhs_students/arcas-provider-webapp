// @flow
const styles = () => ({
  // eslint-disable-line
  loading: {
    paddingTop: "15%",
    paddingLeft: "50%",
    minHeight: "100vh",
    backgroundColor: "#ffffff"
  }
});

export default styles;
