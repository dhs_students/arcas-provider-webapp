// @flow
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer } from "mobx-react";
import { Grid, Hidden } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import MaterialCard from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import LocationIcon from "@material-ui/icons/LocationOn";
import NoImageIcon from "media/images/add-location.png";
import ArcasLogo from "media/images/arcas-logo-2.png";
import { FadeInLeftAnimation } from "common/animations";
import styles from "./styles";

type Props = {
  classes: any,
  title: string,
  subtitle: string,
  image: any,
  location: any,
  startDate: any,
  endDate: any,
  avatar: any,
  onClick: any
};

@observer
class SupplierOfferCard extends React.Component<Props> {
  cardImage = () => {
    if (this.props.image == null) {
      return NoImageIcon;
    }
    return this.props.image;
  };

  orgLogo = () => {
    if (!this.props.avatar || this.props.avatar.length === 0) {
      return ArcasLogo;
    }
    return this.props.avatar;
  };

  render() {
    const { classes, title, onClick } = this.props;
    const image = this.cardImage();
    const card = (
      <div>
        <Hidden smUp>
          <MaterialCard square>
            <CardActionArea onClick={onClick}>
              <CardMedia
                className={classes.normalMedia}
                image={image}
                title={title}
              />
              {this.cardContent()}
            </CardActionArea>
          </MaterialCard>
        </Hidden>
        <Hidden xsDown>
          <CardActionArea onClick={onClick}>
            <MaterialCard className={classes.card} square>
              <CardMedia
                className={classes.cover}
                image={image}
                title={title}
              />
              <div className={classes.details}>{this.cardContent()}</div>
            </MaterialCard>
          </CardActionArea>
        </Hidden>
      </div>
    );
    return (
      <Grid item xs={12}>
        <FadeInLeftAnimation>{card}</FadeInLeftAnimation>
      </Grid>
    );
  }

  cardContent = () => {
    const logo = this.orgLogo();
    const {
      classes,
      title,
      subtitle,
      location,
      startDate,
      endDate
    } = this.props;
    return (
      <CardContent className={classes.content}>
        <div className={classes.orgContainer}>
          <img src={logo} alt="Org Logo" className={classes.orgLogo} />
          <div className={classes.cardOrg}>
            <Typography variant="subtitle2">{title}</Typography>
            <Typography variant="body2" color="textSecondary">
              {subtitle}
            </Typography>
          </div>
        </div>
        <Grid container className={classes.dateLocationContainer}>
          <Grid item xs={12} className={classes.locationContainer}>
            <LocationIcon />
            <Typography
              className={classes.cardOrg}
              variant="body2"
              color="textSecondary"
            >
              {location}
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.dateContainer}>
            <Typography
              className={classes.cardOrg}
              variant="body2"
              color="textSecondary"
            >
              {startDate} - {endDate}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    );
  };
}

export default withStyles(styles)(SupplierOfferCard);
