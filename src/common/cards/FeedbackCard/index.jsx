// @flow
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer } from "mobx-react";
import { Grid } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import MaterialCard from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import StarIcon from "@material-ui/icons/StarOutlined";
import moment from "moment";
import styles from "./styles";

type Props = {
  classes: any,
  wouldReturn: boolean,
  description: string,
  rating: any,
  timestamp: any
};

@observer
class FeedbackCard extends React.Component<Props> {
  render() {
    const { classes } = this.props;
    return (
      <Grid item xs={12}>
        <MaterialCard className={classes.card}>
          <div className={classes.details}>{this.cardContent()}</div>
        </MaterialCard>
      </Grid>
    );
  }

  renderStarIcons = () => {
    const { classes, rating } = this.props;
    const stars = [];
    if (rating != null) {
      for (let i = 1; i <= 5; i += 1) {
        let style = classes.starIconActive;
        if (i > Math.floor(rating)) {
          style = classes.starIconHidden;
        }
        stars.push(<StarIcon key={`star-${i}`} className={style} />);
      }
    }
    return stars;
  };

  cardContent = () => {
    const { classes, description, timestamp, wouldReturn } = this.props;
    return (
      <CardContent className={classes.content}>
        <Grid container>
          <Grid item sm={9} xs={12} className={classes.ratingContainer}>
            {this.renderStarIcons()}
          </Grid>
          <Grid item sm={3} xs={12}>
            <Typography className={classes.wouldReturnText} variant="body2">
              Would return? <b>{wouldReturn}</b>
            </Typography>
          </Grid>
        </Grid>
        <div className={classes.descriptionContainer}>
          <Typography variant="subtitle2">
            {moment(timestamp).format("DD MMMM YYYY")}
          </Typography>
          <div>
            <Typography variant="body2" color="textSecondary">
              {description}
            </Typography>
          </div>
        </div>
      </CardContent>
    );
  };
}

export default withStyles(styles)(FeedbackCard);
