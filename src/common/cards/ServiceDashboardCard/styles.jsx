// @flow
const capacityButton = color => ({
  color: "#fff",
  backgroundColor: color,
  "&:hover": {
    opacity: 0.9,
    backgroundColor: color
  }
});

const styles = (theme: any) => ({
  buttonContainer: {
    textAlign: "right"
  },
  dashboardCount: {
    color: "black"
  },
  barchart: {
    width: 200,
    height: 60
  },
  serviceIcon: {
    margin: 12
  },
  serviceCard: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginTop: 10
  },
  serviceSwitch: {
    padding: 7,
    margin: 0
  },
  allServiceCard: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginTop: 10,
    backgroundColor: "#039be5"
  },
  serviceCardText: {},
  serviceCardContainer: {
    marginBottom: 20,
    padding: 20
  },
  fullButton: {
    ...capacityButton("#f44336")
  },
  mediumButton: {
    ...capacityButton("#ff5722")
  },
  lowButton: {
    ...capacityButton("#4caf50")
  },
  naButton: {
    ...capacityButton("#757575")
  },
  slider: {},
  serviceCardHeading: {
    paddingTop: 10,
    paddingLeft: 20
  },
  trafficContainer: {
    padding: 20
  },
  sliderContainer: {
    paddingTop: 10
  },
  leftPadding: {
    paddingLeft: 30
  }
});

export default styles;
