// @flow
import { observable, computed, action } from "mobx";
import { db } from "../firebase";

type Message = {
  id: string,
  title: string,
  description: string,
  timestamp: TimeStamp,
  organisation: string,
  user: string,
  userAgent: string
};

class ActivityLogsStore {
  path = "cms-activity";

  @observable
  allMessages: Array<Message>;

  @action
  getAllMessages() {
    db.collection(this.path).onSnapshot(collection => {
      this.allMessages = collection.docs.map(doc => {
        const o = { ...doc.data(), ...{ id: doc.id } };
        return o;
      });
    });
  }

  @computed
  get messages(): Array<Message> {
    if (!this.allMessages) {
      this.getAllMessages();
    }
    return this.allMessages;
  }
}

const messages = new ActivityLogsStore();
export default messages;
