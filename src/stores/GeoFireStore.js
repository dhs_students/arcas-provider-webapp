// @flow
import firebase from "firebase";
import { action, observable, computed } from "mobx";
import { GeoFirestore } from "geofirestore";
import { db } from "../firebase";

class GeoFireStore {
  @observable
  data: Array = [];

  @observable
  loading = false;

  @action
  addData(
    collectionName: string,
    longitude: number,
    latitude: number,
    key: string,
    data: any
  ) {
    const supplierRef = db.collection(collectionName);
    const geoFirestore = new GeoFirestore(supplierRef);

    geoFirestore.set(key, {
      coordinates: new firebase.firestore.GeoPoint(latitude, longitude),
      data
    });
  }

  @action
  deleteData(collectionName: string, key: string) {
    const supplierRef = db.collection(collectionName);
    const geoFirestore = new GeoFirestore(supplierRef);

    geoFirestore.remove(key);
  }

  @action
  filterData(
    collectionName: string,
    longitude: number,
    latitude: number,
    radius: number,
    suburb: string,
    searchString: string
  ) {
    this.loading = true;

    const supplierRef = db.collection(collectionName);
    const geoFirestore = new GeoFirestore(supplierRef);

    const geoQuery = geoFirestore.query({
      center: new firebase.firestore.GeoPoint(latitude, longitude),
      radius
    });

    geoQuery.on("ready", () => {
      this.loading = false;
    });
    // Then listen for results as they come in
    geoQuery.on("key_entered", (key, document) => {
      // check whether filtering on keywords
      if (
        searchString.length > 0 &&
        !document.title.toUpperCase().includes(searchString.toUpperCase()) &&
        !document.description.toUpperCase().includes(searchString.toUpperCase())
      ) {
        return;
      }

      const id = key;
      const newDoc = { id, ...document };

      this.data.push(newDoc);
    });
  }

  @computed
  get allData() {
    return this.data;
  }

  @action
  deleteAllData() {
    this.data = [];
  }

  @computed
  get isLoading() {
    return this.loading;
  }
}

const store = new GeoFireStore();
export default store;
