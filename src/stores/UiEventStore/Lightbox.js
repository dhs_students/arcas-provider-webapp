// @flow
import React from "react";
import { observable, action, computed } from "mobx";
import Carousel, { Modal, ModalGateway } from "react-images";

type Image = {
  src: string
};

class Lightbox {
  @observable
  isOpen: boolean;

  @observable
  currentImage: number;

  @observable
  images: Array<Image>;

  constructor() {
    this.isOpen = false;
    this.currentImage = 0;
  }

  @action
  closeLightbox = () => {
    this.isOpen = false;
    this.images = [];
  };

  @action
  openLightbox = (images: Array<Image>) => {
    this.isOpen = true;
    this.currentImage = 0;
    this.images = images;
  };

  @action
  onNext = () => {
    this.currentImage += 1;
  };

  @action
  onBack = () => {
    this.currentImage -= 1;
  };

  @computed
  get lightboxComponent() {
    const { images, isOpen, closeLightbox } = this;
    return (
      <ModalGateway>
        {isOpen ? (
          <Modal styles={{ zIndex: 50 }} onClose={closeLightbox}>
            <Carousel views={images} />
          </Modal>
        ) : null}
      </ModalGateway>
    );
  }
}

export default Lightbox;
