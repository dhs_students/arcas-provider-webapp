// @flow
import firebase from "firebase";
import { observable, action } from "mobx";
import FirebaseUtils from "util/FirebaseUtils";
import PostcodeStore from "stores/PostcodeStore";
import { GeoFirestore } from "geofirestore";
import { db } from "../firebase";
import OrgStore from "./OrgStore";

const supplierOffersPath = "supplier-offers";

class SupplierOfferStore extends OrgStore {
  @observable
  editingOffer: any;

  constructor() {
    super(supplierOffersPath);
    this.editingOffer = null;
  }

  @action
  async removeOffer(offerId: string) {
    this.isUpdatingData = true;
    try {
      await FirebaseUtils.updateDocument(this.collectionPath, offerId, {
        "d.deleted": true
      });
      this.isUpdatingData = false;
    } catch (e) {
      this.isUpdatingData = false;
    }
  }

  @action
  async createOrUpdate(data: Object, existingKey: ?string) {
    const ref = this.getCollectionReference(existingKey);
    const orgDocumentRef = FirebaseUtils.getDocReference(
      `organisation/${data.organisation}`
    );
    this.isUpdatingData = true;
    try {
      const uploadImages = await this.uploadSupplierImages(ref.id, data.images);
      const offerData = {
        title: data.title,
        description: data.description,
        startDate: new Date(data.startDate),
        endDate: new Date(data.endDate),
        location: data.location,
        email: data.email,
        images: uploadImages,
        phone: data.phone,
        organisation: orgDocumentRef,
        orgName: data.orgName,
        orgAvatar: data.orgAvatar,
        deleted: false,
        dateListed: new Date().getTime(),
        updatedAt: new Date()
      };
      const geoCode = await PostcodeStore.getLocation(offerData.location, "SA");
      if (geoCode) {
        return this.addData(
          supplierOffersPath,
          geoCode.longitude,
          geoCode.latitude,
          ref.id,
          offerData
        );
      }
      throw new Error("Invalid Geocode");
    } catch (e) {
      return Promise.reject();
    }
  }

  @action
  async addData(
    collectionName: string,
    longitude: number,
    latitude: number,
    key: string,
    data: any
  ) {
    const supplierRef = db.collection(collectionName);
    const geoFirestore = new GeoFirestore(supplierRef);
    try {
      await geoFirestore.set(key, {
        coordinates: new firebase.firestore.GeoPoint(latitude, longitude),
        ...data
      });
      this.isUpdatingData = false;
    } catch (error) {
      this.isUpdatingData = false;
    }
  }

  @action
  uploadSupplierImages(ref: string, images: Array<any>) {
    return FirebaseUtils.uploadMultipleImages(
      `supplier-offers/${ref}/offerimage-`,
      images
    );
  }

  @action
  async getMyOffers(orgName: string): Promise<any> {
    this.myData = null;
    const snapshot = await FirebaseUtils.createReferenceWhere(
      "supplier-offers",
      [
        {
          key: "d.deleted",
          comparator: "==",
          value: false
        },
        {
          key: "d.orgName",
          comparator: "==",
          value: orgName
        }
      ]
    ).get();
    let docs = [];
    if (snapshot.docs) {
      docs = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data().d
      }));
    }
    this.myData = docs;
  }

  @action
  async getSubsetMyOffers(orgName: string, quantity: number): Promise<any> {
    this.myData = null;
    const snapshot = await FirebaseUtils.createReferenceWhereWithLimitOrder(
      "supplier-offers",
      [
        {
          key: "d.deleted",
          comparator: "==",
          value: false
        },
        {
          key: "d.orgName",
          comparator: "==",
          value: orgName
        }
      ],
      quantity,
      "d.startDate"
    ).get();
    let docs = [];
    if (snapshot.docs) {
      docs = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data().d
      }));
    }
    this.myData = docs;
  }

  @action
  async getAllOffers() {
    const snapshot = await FirebaseUtils.createReferenceWhere(
      "supplier-offers",
      [
        {
          key: "d.deleted",
          comparator: "==",
          value: false
        }
      ]
    ).get();
    let docs = [];
    if (snapshot.docs) {
      docs = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data().d
      }));
    }
    this.allData = docs;
  }

  @action
  async getSubsetAllOffers(quantity: number) {
    const snapshot = await FirebaseUtils.createReferenceWhereWithLimitOrder(
      "supplier-offers",
      [
        {
          key: "d.deleted",
          comparator: "==",
          value: false
        }
      ],
      quantity,
      "d.startDate"
    ).get();
    const documents = [];
    snapshot.forEach(doc => {
      const data = { id: doc.id, ...doc.data().d };
      documents.push(data);
    });
    this.allData = documents;
  }

  resetOffers() {
    this.editingOffer = null;
    this.myData = null;
    this.allData = null;
  }
}

const sOffers = new SupplierOfferStore();
export default sOffers;
