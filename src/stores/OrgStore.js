// @flow
import { observable, action } from "mobx";
import FirebaseUtils from "util/FirebaseUtils";
import { db } from "../firebase";

/**
 * Common Store that implements common/repeatable actions
 */
class OrgStore {
  @observable
  isUpdatingData: boolean;

  @observable
  isGettingData: boolean;

  @observable
  collectionPath: string;

  @observable
  myData: ?Array<any>;

  @observable
  allData: Array<any> | null;

  @observable
  organisation: any;

  constructor(collectionPath: string) {
    this.collectionPath = collectionPath;
    this.resetStore();
  }

  resetStore() {
    this.isUpdatingData = false;
    this.isGettingData = false;
    this.myData = [];
    this.allData = [];
  }

  /**
   * Retrieves a collection reference based on an existing key, if none exists a new reference will be created
   */
  getCollectionReference(existingKey: ?string) {
    if (existingKey) {
      return db.collection(this.collectionPath).doc(existingKey);
    }
    return db.collection(this.collectionPath).doc();
  }

  @action
  resetMyData() {
    this.myData = null;
  }

  @action
  resetAllData() {
    this.allData = null;
  }

  @action
  getDocument(docId: string): Promise<any> {
    this.isGettingData = true;
    return FirebaseUtils.getDocument(this.collectionPath, docId).then(
      (doc: any) => {
        this.isGettingData = false;
        this.organisation = doc.data();
        return doc;
      }
    );
  }
}

export default OrgStore;
