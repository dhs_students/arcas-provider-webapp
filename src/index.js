import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "mobx-react";
import registerServiceWorker from "./registerServiceWorker";
import AuthStore from "./stores/AuthStore";
import UserStore from "./stores/UserStore";
import LocationStore from "./stores/LocationStore";
import SupplierOfferStore from "./stores/SupplierOfferStore";
import ActivityLogsStore from "./stores/ActivityLogsStore";
import RefStore from "./stores/RefStore";
import GeoFireStore from "./stores/GeoFireStore";
import OrgStore from "./stores/OrgStore";
import PostcodeStore from "./stores/PostcodeStore";
import UiEventStore from "./stores/UiEventStore";
import App from "./App";

const stores = {
  AuthStore,
  UserStore,
  LocationStore,
  SupplierOfferStore,
  ActivityLogsStore,
  RefStore,
  UiEventStore,
  OrgStore,
  GeoFireStore,
  PostcodeStore
};

ReactDOM.render(
  <Provider {...stores}>
    <div>
      <App />
    </div>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
