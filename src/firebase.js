import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";
import "firebase/firestore";
import "firebase/functions";

const devConfig = {
  apiKey: "AIzaSyCMc1IfJWpplqUl7RmZYrUuWQVgYcp0S1U",
  authDomain: "arcas-bd925.firebaseapp.com",
  databaseURL: "https://arcas-bd925.firebaseio.com",
  projectId: "arcas-bd925",
  storageBucket: "arcas-bd925.appspot.com",
  messagingSenderId: "53025405141"
};

if (!firebase.apps.length) {
  firebase.initializeApp(devConfig);
}

const auth = firebase.auth();
const database = firebase.database();
const db = firebase.firestore();
const storage = firebase.storage();
const functions = firebase.functions();

export { firebase, auth, database, db, storage, functions };
