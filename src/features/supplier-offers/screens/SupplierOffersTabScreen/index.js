// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { action } from "mobx";
import { withStyles } from "@material-ui/core/styles";
import DashboardTabLayout from "common/layouts/DashboardTabLayout";
import history from "appHistory";
import LocalOffer from "@material-ui/icons/LocalOfferOutlined";
import PublicOffersScreen from "../PublicOffersScreen";
import styles from "./styles";
import MyOffersScreen from "../MyOffersScreen";

type Props = {
  SupplierOfferStore: any,
  location: any
};

@inject("AuthStore", "UserStore", "SupplierOfferStore")
@observer
class SupplierOffersTabScreen extends Component<Props> {
  render() {
    return (
      <DashboardTabLayout
        tabs={[
          { label: "My Offers", component: MyOffersScreen, path: "#myoffers" },
          {
            label: "Public Offers",
            component: PublicOffersScreen,
            path: "#alloffers"
          }
        ]}
        location={this.props.location}
        headerIcon={LocalOffer}
        actionButton={{
          label: "Add new offer",
          onClick: () => this.addNewOffer()
        }}
        pageTitle="Supplier Offers"
      />
    );
  }

  @action
  resetSupplierOffers = () => {
    const { SupplierOfferStore } = this.props;
    SupplierOfferStore.resetOffers();
  };

  @action
  addNewOffer() {
    history.push("/supplieroffers/add");
  }
}

export default withStyles(styles)(SupplierOffersTabScreen);
