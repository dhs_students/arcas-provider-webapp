// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { TextField } from "@material-ui/core";
import Select from "react-select";
import { observer, inject } from "mobx-react";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { SupplierOfferCard } from "common/cards";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/SearchOutlined";
import Slider from "@material-ui/core/Slider";
import CardViewTemplate from "common/layouts/CardViewTemplate";
import LoadingScreen from "common/screens/LoadingScreen";
import { FadeInAnimation } from "common/animations";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any,
  loading: any,
  SupplierOfferStore: any
};

@inject("AuthStore", "SupplierOfferStore")
@observer
class PublicOffersView extends Component<Props> {
  render() {
    const { classes, controller, loading } = this.props;
    const {
      supplierObjects,
      didViewOffer,
      handleSliderChange,
      marks,
      handleSearchStringChange,
      suburbPostcodeOptions,
      filters,
      handleSuburbChange
    } = controller;
    const { suburb, searchString, distance } = filters;
    const cards = supplierObjects.map(offer => (
      <SupplierOfferCard
        key={offer.id}
        {...offer}
        onClick={() => didViewOffer(offer.id)}
      />
    ));
    if (loading) {
      return <LoadingScreen />;
    }
    return (
      <CardViewTemplate
        header={
          <div className={classes.headerDescription}>
            <Typography
              variant="subtitle1"
              component="h2"
              style={{ display: "inline-block" }}
            >
              {controller.resultsHeading}
            </Typography>
          </div>
        }
        cards={cards}
        cardsPerPage={5}
        sideBarButtonTitle="Filter Results"
        sideBarContent={
          <FadeInAnimation>
            <div className={classes.filterSearchContainer}>
              <Typography variant="subtitle2">Refine your search</Typography>
              <TextField
                id="outlined-simple-start-adornment"
                className={classes.filterSearchTextField}
                variant="outlined"
                label="Search Offers"
                autoComplete="search"
                margin="normal"
                fullWidth
                value={searchString}
                onChange={handleSearchStringChange}
                onKeyPress={event => {
                  const code = event.keyCode || event.which;
                  if (code === 13) {
                    // 13 is the enter keycode
                    controller.applyFilters();
                  }
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      {" "}
                      <SearchIcon fontSize="small" />
                    </InputAdornment>
                  )
                }}
              />
            </div>
            <div className={classes.filterLocationContainer}>
              <Typography variant="subtitle2">Location</Typography>
              <Select
                className={classes.locationField}
                menuContainerStyle={{ zIndex: 100 }}
                inputContainer
                options={suburbPostcodeOptions}
                value={suburb}
                isMulti
                onChange={handleSuburbChange}
                placeholder="Select a suburb or postcode"
                isSearchable
                isClearable
                zindex="5"
              />
            </div>
            <div>
              <Typography variant="subtitle2">
                Include offers within {filters.distance} km
              </Typography>
              <div>
                <Slider
                  className={classes.sliderControl}
                  defaultValue={20}
                  value={distance}
                  aria-labelledby="discrete-slider-restrict"
                  onChange={handleSliderChange}
                  step={null}
                  valueLabelDisplay="auto"
                  marks={marks}
                />
              </div>
            </div>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submitButton}
              onClick={() => controller.applyFilters()}
            >
              Apply filters
            </Button>
            <Button
              color="secondary"
              className={classes.clearFilter}
              onClick={() => controller.clearFilter()}
            >
              Reset
            </Button>
          </FadeInAnimation>
        }
      />
    );
  }
}

export default withStyles(styles)(PublicOffersView);
