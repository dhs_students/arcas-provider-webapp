// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer, inject } from "mobx-react";
import { Typography, Grid, Avatar, Paper } from "@material-ui/core";
import withWidth from "@material-ui/core/withWidth";
import styles from "./styles";

type Props = {
  classes: PropTypes.object,
  controller: PropTypes.object
};

@inject("AuthStore", "UserStore")
@observer
class ViewOfferView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const {
      hasImages,
      orgAvatar,
      toggleLightbox,
      offerImage,
      imageCount,
      offer,
      startDate,
      endDate
    } = controller;
    const { title, orgName, location, email, phone } = offer;
    return (
      <Grid container className={classes.pageContainer}>
        <Grid item xs={12} className={classes.headingContainer}>
          <Typography variant="h5" component="h6">
            {title}
          </Typography>
        </Grid>
        <Grid item xs={12} className={classes.orgContainer}>
          <Avatar src={orgAvatar} />
          <Typography
            variant="body2"
            color="textSecondary"
            className={classes.orgTitle}
          >
            {orgName}
          </Typography>
        </Grid>
        {hasImages && (
          <Grid
            item
            xs={12}
            sm={12}
            md={7}
            className={classes.imageGalleryContainer}
            onClick={toggleLightbox}
          >
            <img
              alt="galleryImageTest"
              src={offerImage}
              className={classes.galleryImage}
            />
            <Paper className={classes.imageCount}>
              <Typography variant="body1">{imageCount}</Typography>
            </Paper>
          </Grid>
        )}
        {!hasImages && this.renderDescription(7)}
        <Grid
          item
          xs={12}
          sm={12}
          md={5}
          className={classes.locationSideContainer}
        >
          <Paper className={classes.locationPaperContainer}>
            <Grid item xs={12} className={classes.mobileContentSection}>
              <Typography
                className={classes.contentSubheading}
                variant="subtitle1"
              >
                Location
              </Typography>
              <Typography variant="body1">{location}</Typography>
            </Grid>
            <Grid item xs={12} className={classes.mobileContentSection}>
              <Typography
                className={classes.contentSubheading}
                variant="subtitle1"
              >
                Valid from
              </Typography>
              <Typography variant="body1">
                {startDate} - {endDate}
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.mobileContentSection}>
              <Typography
                className={classes.contentSubheading}
                variant="subtitle1"
              >
                Contact Number
              </Typography>
              <Typography variant="body1">{phone}</Typography>
            </Grid>
            {email !== "" && (
              <Grid item xs={12} className={classes.mobileContentSection}>
                <Typography
                  className={classes.contentSubheading}
                  variant="subtitle1"
                >
                  Contact Email
                </Typography>
                <Typography variant="body1">{email}</Typography>
              </Grid>
            )}
          </Paper>
        </Grid>
        {hasImages && this.renderDescription(12)}
      </Grid>
    );
  }

  renderDescription = (size: number) => {
    const { classes, controller } = this.props;
    const { description } = controller;
    return (
      <Grid item xs={size} className={classes.descriptionContainer}>
        <Typography className={classes.contentSubheading} variant="subtitle1">
          Offer Description
        </Typography>
        <Typography variant="body1">{description}</Typography>
      </Grid>
    );
  };
}

export default withWidth()(withStyles(styles)(ViewOfferView));
