// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { computed, action, observable } from "mobx";
import DashboardTabLayout from "common/layouts/DashboardTabLayout";
import OrgSettingsView from "features/settings/components/OrganisationSettingsView";
import PersonalSettingsView from "features/settings/components/PersonalSettingsView";

type Props = {
  UserStore: any,
  AuthStore: any,
  UiEventStore: any
};

@inject("AuthStore", "UserStore", "UiEventStore", "OrgStore")
@observer
class SettingsScreen extends Component<Props> {
  @observable
  openModals: {
    isUpdatingDisplayPicture: boolean,
    isChangingPassword: boolean
  };

  @observable
  changePasswordLoading: boolean;

  @observable
  openDialogTitle: string;

  constructor(props: Object) {
    super(props);
    this.openModals = {
      isUpdatingDisplayPicture: false,
      isChangingPassword: false
    };
    this.changePasswordLoading = false;
    this.bindMethods();
  }

  bindMethods() {
    this.closePasswordModal = this.closePasswordModal.bind(this);
    this.openPasswordModal = this.openPasswordModal.bind(this);
    this.onTabChange = this.onTabChange.bind(this);
  }

  render() {
    return (
      <DashboardTabLayout
        pageTitle="Settings"
        tabs={[
          {
            label: "Organisation",
            component: OrgSettingsView,
            path: "#organisation"
          },
          {
            label: "Personal",
            component: PersonalSettingsView,
            path: "#personal"
          }
        ]}
        onTabChange={this.onTabChange}
      />
    );
  }

  @action
  handleDialogClose() {
    Object.keys(this.openModals).forEach(key => {
      this.openModals[key] = false;
    });
  }

  @action
  closePasswordModal = () => {
    this.openModals.isChangingPassword = false;
  };

  @action
  openPasswordModal = () => {
    this.openModals.isChangingPassword = true;
  };

  @action
  closeDisplayPictureModal = () => {
    this.openModals.isUpdatingDisplayPicture = false;
  };

  @action
  openDisplayPictureModal = () => {
    this.openModals.isUpdatingDisplayPicture = true;
  };

  @action
  handleSubmitImage = (imagePreviewFile: any) => {
    const { UserStore } = this.props;
    UserStore.uploadAvatar(imagePreviewFile).then(() => {
      this.closeDisplayPictureModal();
    });
  };

  @action
  onChangePasswordSuccess = (form: any) => {
    const { AuthStore, UserStore, UiEventStore } = this.props;
    const { email } = UserStore.user;
    const { existingPassword, password } = form.values();
    this.changePasswordLoading = true;
    AuthStore.doChangePassword(email, existingPassword, password)
      .then(() => {
        this.changePasswordLoading = false;
        UiEventStore.triggerSnackbarSuccess("Password changed successfully.");
        this.closePasswordModal();
      })
      .catch(() => {
        UiEventStore.triggerSnackbarFail("Wrong existing password entered.");
        this.changePasswordLoading = false;
      });
  };

  @action
  onChangePasswordFail = () => {};

  @computed
  get changePasswordFields(): Array<any> {
    return [
      {
        name: "existingPassword",
        type: "password",
        label: "Existing Password",
        rules: "required|string"
      },
      {
        name: "password",
        type: "password",
        label: "New Password",
        rules: "required|string|between:5,25"
      },
      {
        name: "passwordConfirm",
        type: "password",
        label: "Password Confirmation",
        rules: "required|string|same:password"
      }
    ];
  }

  @action
  openEditDialog = (dialogName: string) => {
    this.openDialogTitle = dialogName;
  };

  @computed
  get user(): Object {
    return this.props.UserStore.user;
  }

  @computed
  get currentOrganisation(): Object {
    return this.props.UserStore.currentOrganisation;
  }

  @action
  onTabChange = (currentTab: number) => {
    if (currentTab === 0) {
      // this.isProfileTab = true;
      // this.isServiceTab = false;
    } else if (currentTab === 1) {
      // this.isServiceTab = true;
      // this.isProfileTab = false;
    } else {
      // this.isServiceTab = false;
      // this.isProfileTab = false;
    }
  };

  @computed
  get editDialogs(): Array<Object> {
    return [
      {
        title: "orgName",
        label: "Organisation Name",
        defaultValue: this.currentOrganisation.orgName,
        rules: "required|string|max:60",
        storeRef: this.props.UserStore.updateName.bind(this)
      },
      {
        title: "abn",
        label: "ABN",
        defaultValue: this.currentOrganisation.abn,
        rules: "required|numeric|digits:11",
        storeRef: this.props.UserStore.updateAbn.bind(this)
      },
      {
        title: "phone",
        label: "Phone",
        defaultValue: this.currentOrganisation.phone,
        rules: "required|numeric|digits:10",
        type: "tel",
        storeRef: this.props.UserStore.updatePhone.bind(this)
      }
    ].map(dialog => {
      const updatedDialog: Object = dialog;
      updatedDialog.onClose = () => {
        this.openDialogTitle = "";
      };
      updatedDialog.onEdit = (newValue: string) => {
        updatedDialog.storeRef(newValue);
        this.openDialogTitle = "";
      };
      updatedDialog.isOpen = this.openDialogTitle === updatedDialog.title;
      return updatedDialog;
    });
  }
}

export default SettingsScreen;
