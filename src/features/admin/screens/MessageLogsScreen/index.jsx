// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { action, observable } from "mobx";
import withStyles from "@material-ui/core/styles/withStyles";
import LoadingScreen from "common/screens/LoadingScreen";
import DashboardLayout from "common/layouts/DashboardLayout";
import MessageLogsView from "./MessageLogsView";
import styles from "./styles";

type Props = {
  ActivityLogsStore: any,
  classes: any
};

@inject("AuthStore", "ActivityLogsStore")
@observer
class MessageLogScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  signUpFailed: ?boolean;

  render() {
    const { classes } = this.props;
    if (this.props.ActivityLogsStore.messages) {
      return (
        <DashboardLayout pageTitle="Activity Logs">
          <div className={classes.root}>
            <div className={classes.panelContainer}>
              <MessageLogsView
                controller={this}
                messages={this.props.ActivityLogsStore.messages}
              />
            </div>
          </div>
        </DashboardLayout>
      );
    }
    return (
      <DashboardLayout>
        <LoadingScreen />
      </DashboardLayout>
    );
  }

  @action
  handleSnackbarClose = (): void => {
    this.signUpFailed = false;
  };
}

export default withStyles(styles)(MessageLogScreen);
