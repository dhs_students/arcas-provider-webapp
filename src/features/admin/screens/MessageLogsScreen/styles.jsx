// @flow
const styles = (theme: any) => ({
  root: {
    [theme.breakpoints.up("lg")]: {
      width: "100%"
    }
  },
  button: {
    margin: theme.spacing(1)
  },
  fab: {
    margin: theme.spacing(3),
    right: 30,
    position: "fixed"
  },
  table: {
    marginBottom: "0",
    width: "100%",
    maxWidth: "100%",
    backgroundColor: "transparent",
    borderSpacing: "0",
    borderCollapse: "collapse"
  },
  tableHeadCell: {
    color: "inherit",
    fontSize: "1em"
  },
  tableCell: {
    lineHeight: "1.42857143",
    padding: "12px 8px",
    verticalAlign: "middle"
  },
  tableResponsive: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  }
});

export default styles;
