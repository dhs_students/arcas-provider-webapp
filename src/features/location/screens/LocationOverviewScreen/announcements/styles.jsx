// @flow
const styles = () => ({
  root: {
    padding: 10
  },
  pageDescription: {
    paddingBottom: 10
  },
  noOffersContainer: {
    textAlign: "center",
    paddingTop: 60,
    paddingBottom: 40,
    paddingLeft: 40,
    paddingRight: 40
  },
  noOffersTitle: {
    paddingBottom: 5
  }
});

export default styles;
