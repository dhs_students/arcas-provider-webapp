// @flow
const styles = () => ({
  layout: {
    padding: 16
  },
  editButton: {
    width: 20,
    height: 20,
    padding: 0
  },
  deleteButton: {
    backgroundColor: "red",
    "&:hover": {
      backgroundColor: "red",
      opacity: 0.9
    }
  },
  gridList: {
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: "translateZ(0)"
  },
  titleBar: {
    background:
      "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
      "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
  },
  deleteIcon: {
    color: "white"
  },
  mapContainer: {
    width: "100%",
    height: "40vh"
  },
  viewImagesContainer: {
    display: "flex",
    justifyContent: "flex-end"
  },
  viewImagesButton: {
    height: 30,
    marginTop: -8
  },
  profileInformationField: {
    paddingBottom: 15
  },
  multilineText: {
    padding: 0,
    margin: 0,
    lineHeight: "normal",
    whiteSpace: "pre-wrap"
  }
});

export default styles;
