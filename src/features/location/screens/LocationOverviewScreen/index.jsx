// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import DashboardTabLayout from "common/layouts/DashboardTabLayout";
import LocationOn from "@material-ui/icons/LocationOnOutlined";
import history from "appHistory";
import FeedbackView from "./feedback";
import LocationProfileView from "./profile";
import LocationServicesView from "./services";
import AnnouncementView from "./announcements";
import styles from "./styles";

type Props = {
  classes: any,
  LocationStore: any,
  UiEventStore: any,
  location: any
};

const Tab = {
  Announcement: "Announcement",
  Services: "Services",
  Profile: "Profile"
};

@inject("LocationStore", "UiEventStore")
@observer
class LocationOverviewScreen extends Component<Props> {
  @observable
  selectedTab: Tab;

  constructor(props: Object) {
    super(props);
    this.selectedTab = Tab.Profile;
  }

  render() {
    return (
      <div>
        <DashboardTabLayout
          actionButton={this.addActionButton}
          pageTitle="My Location"
          headerIcon={LocationOn}
          location={this.props.location}
          tabs={[
            {
              label: "Profile",
              component: LocationProfileView,
              path: "#profile"
            },
            {
              label: "Services",
              component: LocationServicesView,
              path: "#services"
            },
            {
              label: "Announcements",
              component: AnnouncementView,
              path: "#announcements"
            },
            {
              label: "Feedback",
              component: FeedbackView,
              path: "#feedback"
            }
          ]}
          onTabChange={this.onTabChange}
        />
      </div>
    );
  }

  @computed
  get addActionButton(): ?Object {
    const { classes } = this.props;
    if (this.isProfileTab) {
      return {
        label: "Delete Location",
        onClick: () => this.confirmDeleteLocation(),
        className: classes.deleteButton
      };
    }
    if (this.isServiceTab) {
      return {
        label: "Add new service",
        onClick: () => this.addNewService()
      };
    }
    if (this.isAnnouncementTab) {
      return {
        label: "Add an announcement",
        onClick: () => this.addNewAnnouncement()
      };
    }
    return null;
  }

  @action
  addNewService = () => {
    history.push("/locations/addservice");
  };

  @action
  addNewAnnouncement = () => {
    history.push("/locations/announcements/add");
  };

  @action
  confirmDeleteLocation = () => {
    this.props.UiEventStore.alertDialog.onOpen({
      title: "Confirm Delete",
      description: "Are you sure you want to delete this Location?",
      onPositiveButtonClicked: async () => {
        const { LocationStore } = this.props;
        LocationStore.isLocationRetrieved = false;
        await LocationStore.deleteLocation();
        history.push("/dashboard");
        LocationStore.isLocationRetrieved = true;
      }
    });
  };

  @action
  onTabChange = (currentTab: number) => {
    if (currentTab === 0) {
      this.selectedTab = Tab.Profile;
    } else if (currentTab === 1) {
      this.selectedTab = Tab.Services;
    } else if (currentTab === 2) {
      this.selectedTab = Tab.Announcement;
    } else {
      this.selectedTab = Tab.Feedback;
    }
  };

  @computed
  get isProfileTab(): boolean {
    return this.selectedTab === Tab.Profile;
  }

  @computed
  get isServiceTab(): boolean {
    return this.selectedTab === Tab.Services;
  }

  @computed
  get isAnnouncementTab(): boolean {
    return this.selectedTab === Tab.Announcement;
  }
}

export default withStyles(styles)(LocationOverviewScreen);
