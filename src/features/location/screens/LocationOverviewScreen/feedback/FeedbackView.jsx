// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { observer } from "mobx-react";
import CardViewTemplate from "common/layouts/CardViewTemplate";
import { Grid, Paper } from "@material-ui/core";
import FeedbackCard from "common/cards/FeedbackCard";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any
};

@observer
class FeedbackView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const { overallRating, cardData, feedbackCount } = controller;
    const cards = cardData.map((x, index) => (
      <FeedbackCard key={`${x.timestamp}-${index + 1}`} {...x} />
    ));
    if (cardData.length === 0) {
      return (
        <Grid container>
          <Grid item xs={12} className={classes.noFeedbackContainer}>
            <Typography
              className={classes.noFeedbackTitle}
              variant="h5"
              component="h2"
            >
              You currently have no feedback.
            </Typography>
            <Typography variant="subtitle1">
              Users of the arcas mobile app can provide feedback of their
              experiences with your location
            </Typography>
          </Grid>
        </Grid>
      );
    }
    return (
      <Grid container>
        <Grid container spacing={10} className={classes.statsContainer}>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.scoreTile}>
              <Typography variant="subtitle2">Overall Score</Typography>
              <Typography variant="h4" component="p">
                {overallRating}
              </Typography>
              <Typography variant="caption">
                Only you can see this score
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.feedbackReceivedTile}>
              <Typography variant="subtitle2">Feedback Received</Typography>
              <Typography variant="h4" component="p">
                {feedbackCount}
              </Typography>
            </Paper>
          </Grid>
        </Grid>

        <Grid item xs={12} className={classes.headerDescription}>
          <Typography variant="h5" component="h2">
            Here is what people have been saying recently
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <CardViewTemplate cards={cards} cardsPerPage={15} disableDrawer />
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(FeedbackView);
