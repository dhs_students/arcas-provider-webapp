// @flow
import React, { Component } from "react";
import { action, computed } from "mobx";
import { inject, observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import ServiceView from "./ServiceView";

type Props = {
  controller: any,
  RefStore?: any
};

@inject("RefStore")
@observer
class ServiceScreen extends Component<Props> {
  render() {
    return (
      <ServiceView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: any) => {
    const { service, handleNext } = this.props.controller;
    const serviceObj = form.values();
    service.category = serviceObj.category;
    service.age = serviceObj.age;
    service.gender = serviceObj.gender;
    handleNext();
  };

  @action
  onError = () => {};

  @action
  handleBack() {
    this.props.controller.handleBack();
  }

  @action
  handleCancel() {
    this.props.controller.handleCancel();
  }

  @computed
  get categories(): Array<Object> {
    const { serviceCategories } = (this.props.RefStore: any);
    return serviceCategories;
  }

  @computed
  get ageReference(): Array<any> {
    const { ageRefs } = (this.props.RefStore: any);
    return ageRefs;
  }

  @computed
  get genderReference(): Array<any> {
    const { genderRefs } = (this.props.RefStore: any);
    return genderRefs;
  }

  @action
  handleCategorySelect = (event: any) => {
    this.service.category = event.target.value;
  };

  @action
  handleAgeChange = (event: any) => {
    this.service.age = event.target.value;
  };

  @action
  handleGenderChange = (event: any) => {
    this.service.gender = event.target.value;
  };

  @computed
  get service(): Object {
    return this.props.controller.service;
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "category",
        type: "select",
        label: "Service Category",
        rules: "required|string",
        value: this.service.category
      },
      {
        name: "age",
        type: "select",
        label: "Age Restriction",
        value: this.service.age
      },
      {
        name: "gender",
        type: "select",
        label: "Gender Restriction",
        value: this.service.gender
      }
    ];
  }
}

export default ServiceScreen;
