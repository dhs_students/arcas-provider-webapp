// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import MenuItem from "@material-ui/core/MenuItem";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as ServiceUtils from "util/ServiceUtils";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any
};

@observer
class ServiceView extends Component<Props> {
  render() {
    const { classes, controller, form } = this.props;
    const { genderReference, ageReference, handleCancel } = controller;
    const category = form.$("category");
    const gender = form.$("gender");
    const age = form.$("age");
    const type = null;
    const errors = form.errors();
    return (
      <React.Fragment>
        <CssBaseline />
        <form className={classes.form} onSubmit={form.onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                select
                variant="outlined"
                {...category.bind({ type })}
                fullWidth
                error={errors.category != null}
                helperText={errors.category}
                onChange={controller.handleCategorySelect}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu
                  }
                }}
              >
                {controller.categories.map(option => (
                  <MenuItem key={option.id} value={option.id}>
                    <FontAwesomeIcon
                      className={classes.serviceIcon}
                      icon={ServiceUtils.getIconForService(option.id)}
                      size="1x"
                    />
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label={age.label}
                fullWidth
                select
                variant="outlined"
                value="test"
                SelectProps={{
                  multiple: true,
                  ...age.bind({ type }),
                  onChange: controller.handleAgeChange,
                  renderValue: selected => selected.join(", "),
                  MenuProps: {
                    className: classes.menu
                  }
                }}
                helperText="Leave blank if no restrictions apply"
              >
                {ageReference.map(ageRef => (
                  <MenuItem key={ageRef.id} value={ageRef.name}>
                    <Checkbox
                      checked={controller.service.age.indexOf(ageRef.name) > -1}
                    />
                    <ListItemText primary={ageRef.name} />
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label={gender.label}
                fullWidth
                select
                variant="outlined"
                SelectProps={{
                  multiple: true,
                  ...gender.bind({ type }),
                  onChange: controller.handleGenderChange,
                  renderValue: selected => selected.join(", "),
                  MenuProps: {
                    className: classes.menu
                  }
                }}
                helperText="Leave blank if no restrictions apply"
              >
                {genderReference.map(genderRef => (
                  <MenuItem key={genderRef.id} value={genderRef.name}>
                    <Checkbox
                      checked={
                        controller.service.gender.indexOf(genderRef.name) > -1
                      }
                    />
                    <ListItemText primary={genderRef.name} />
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
          </Grid>
          <React.Fragment>
            <div className={classes.buttons}>
              <Button className={classes.button} onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
              >
                Next
              </Button>
            </div>
          </React.Fragment>
        </form>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ServiceView);
