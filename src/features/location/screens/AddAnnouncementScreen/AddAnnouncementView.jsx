// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { Grid, Button, TextField, Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any
};

@observer
class AddAnnouncementView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const { handleCancel, submitButtonText, form, didChangeEvent } = controller;
    const title = form.$("title");
    const description = form.$("description");
    const eventDate = form.$("eventDate");
    const startTime = form.$("startTime");
    const endTime = form.$("endTime");
    const isEvent = form.$("isEvent");
    const type = null;
    const errors = form.errors();
    return (
      <React.Fragment>
        <Grid container>
          <div className={classes.descriptionContainer}>
            <Typography variant="h5" component="h2">
              Please fill out the following information
            </Typography>
            <Typography variant="body1" color="textSecondary">
              It is recommended that you make the title descriptive and keep the
              description short to help engage people.
            </Typography>
          </div>

          <form onSubmit={form.onSubmit} className={classes.formContainer}>
            <Grid item xs={12}>
              <TextField
                {...title.bind({ type })}
                variant="outlined"
                margin="normal"
                fullWidth
                helperText={errors.title}
                error={errors.title != null}
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                {...description.bind({})}
                variant="outlined"
                multiline
                rows="4"
                rowsMax="8"
                margin="normal"
                helperText={errors.description}
                error={errors.description != null}
                fullWidth
              />
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={4}>
                <TextField
                  {...eventDate.bind({})}
                  helperText={errors.eventDate}
                  type="date"
                  error={errors.eventDate != null}
                  InputLabelProps={{ shrink: true }}
                  variant="outlined"
                  margin="normal"
                  fullWidth
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    {...isEvent.bind({ type: "checkbox" })}
                    checked={isEvent.value}
                    color="primary"
                  />
                }
                label={isEvent.label}
              />
            </Grid>
            {isEvent.value === true && (
              <Grid container spacing={2}>
                <Grid item xs={12} sm={4}>
                  <TextField
                    {...startTime.bind({})}
                    type="time"
                    InputLabelProps={{ shrink: true }}
                    error={errors.startTime != null}
                    helperText={errors.startTime}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <TextField
                    {...endTime.bind({})}
                    type="time"
                    InputLabelProps={{ shrink: true }}
                    error={errors.endTime != null}
                    helperText={errors.endTime}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                  />
                </Grid>
              </Grid>
            )}
            <Grid item xs={12} className={classes.stepperButtonContainer}>
              <Button onClick={handleCancel}>Cancel</Button>
              <Button type="submit" variant="contained" color="primary">
                {submitButtonText}
              </Button>
            </Grid>
          </form>
        </Grid>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(AddAnnouncementView);
