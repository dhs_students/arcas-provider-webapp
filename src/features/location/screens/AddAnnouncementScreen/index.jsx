// @flow
import React, { Component } from "react";
import { action, observable, computed, autorun } from "mobx";
import { observer, inject } from "mobx-react";
import DashboardOverlapLayout from "common/layouts/DashboardOverlapLayout";
import LocationIcon from "@material-ui/icons/LocationOn";
import LoadingScreen from "common/screens/LoadingScreen";
import history from "appHistory";
import moment from "moment";
import MobxForm from "util/MobxForm";
import AddAnnouncementView from "./AddAnnouncementView";

const Validator = require("validatorjs");

type Props = {
  LocationStore: any,
  UiEventStore: any,
  UserStore: any,
  location: any,
  match: any
};

@inject("LocationStore", "UiEventStore", "RefStore", "UserStore")
@observer
class AddAnnouncementScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  isLoading: boolean;

  @observable
  showCancelDialog: boolean;

  @observable
  form: any;

  @observable
  existingAnnouncement = null;

  @observable
  dataFetched = false;

  @observable
  allDayEvent = true;

  constructor(props: Props) {
    super(props);
    this.isLoading = false;
    this.showCancelDialog = false;
  }

  componentDidMount() {
    this.setupForm();
    this.setupInitialData().then(() => {
      if (this.isEditing && !this.existingAnnouncement) {
        history.push("/locations/overview#announcements");
      }
      this.setupForm();
      this.dataFetched = true;
    });
  }

  @computed
  get isEditing(): boolean {
    const { location } = this.props;
    return location.pathname.includes("edit");
  }

  setupInitialData = () => {
    const { LocationStore } = this.props;
    if (this.isEditing) {
      return LocationStore.getAnnouncement(this.props.match.params.id)
        .then(doc => {
          this.existingAnnouncement = { ...doc.data(), id: doc.id };
        })
        .catch(() => {
          history.push("/locations/overview#announcements");
        });
    }
    return Promise.resolve();
  };

  render() {
    const { LocationStore } = this.props;
    return (
      <DashboardOverlapLayout
        pageTitle={this.isEditing ? "Edit Announcement" : "Add Announcement"}
        headerIcon={LocationIcon}
      >
        {LocationStore.isUpdatingData || !this.dataFetched ? (
          <LoadingScreen />
        ) : (
          <AddAnnouncementView controller={this} />
        )}
      </DashboardOverlapLayout>
    );
  }

  setupForm = () => {
    Validator.register(
      "start_before_end",
      startTime => {
        const { endTime, isEvent } = this.form.values();
        if (isEvent === false) {
          return true;
        }
        return this.timeIsBefore(startTime, endTime) === true;
      },
      "Must be before end time"
    );
    Validator.register(
      "end_after_start",
      endTime => {
        const { startTime, isEvent } = this.form.values();
        if (isEvent === false) {
          return true;
        }
        return this.timeIsBefore(endTime, startTime) === false;
      },
      "Must be after start time"
    );
    const fields = [
      {
        name: "title",
        label: "Title",
        rules: "required|string|between:3,60",
        value: ""
      },
      {
        name: "description",
        label: "Description",
        rules: "required|string|max:500",
        value: ""
      },
      {
        name: "today",
        type: "date",
        label: "Current Date",
        value: moment(new Date()).format("YYYY-MM-DD")
      },
      {
        name: "eventDate",
        type: "date",
        label: "Date of Event",
        rules: "required|date|after_or_equal:today",
        value: ""
      },
      {
        name: "isEvent",
        type: "checkbox",
        label: "This announcement is an event"
      },
      {
        name: "startTime",
        type: "time",
        label: "Start time",
        rules: [{ required_if: ["isEvent", true] }, "start_before_end"],
        value: ""
      },
      {
        name: "endTime",
        type: "time",
        label: "End time",
        rules: [{ required_if: ["isEvent", true] }, "end_after_start"],
        value: ""
      }
    ];

    this.form = new MobxForm(fields, this.onFormSuccess, this.onFormError);
    this.form.prefillData({ ...this.existingAnnouncement });
  };

  @action
  onFormSuccess = () => {
    const { UserStore, LocationStore } = this.props;
    const orgDetails = {
      orgName: UserStore.currentOrganisation.orgName,
      orgAvatar: UserStore.currentOrganisation.avatar
        ? UserStore.currentOrganisation.avatar
        : "",
      organisation: UserStore.currentOrganisation.id
    };
    const formData = this.form.values();
    if (formData.isEvent === false) {
      formData.startTime = "";
      formData.endTime = "";
    }
    LocationStore.createOrUpdateAnnouncement(
      {
        ...formData,
        ...orgDetails
      },
      this.existingAnnouncement ? this.existingAnnouncement.id : null,
      LocationStore.currentLocation
    ).then(() => {
      history.push("/locations/overview#announcements");
    });
  };

  timeIsBefore = (startTime: string, endTime: string): boolean => {
    const startVal = startTime;
    const endVal = endTime;

    const startMoment = moment(startVal, "HH:mm");
    const endMoment = moment(endVal, "HH:mm");

    return startMoment.isBefore(endMoment);
  };

  @action
  onFormError = () => {
    console.log(this.form.errors());
    console.log(this.form.values());
  };

  @computed
  get submitButtonText(): string {
    if (this.isEditing) {
      return "Update announcement";
    }
    return "Create new announcement";
  }

  handleCancel = () => {
    this.props.UiEventStore.alertDialog.onOpen({
      title: "Return to your announcements",
      description: "Do you want to go back?",
      onPositiveButtonClicked: () => {
        history.push("/locations/overview#announcements");
      }
    });
  };
}

export default AddAnnouncementScreen;
