// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any
};

@observer
class LocationAddressView extends Component<Props> {
  render() {
    const { classes, controller, form } = this.props;
    const address1 = form.$("address1");
    const address2 = form.$("address2");
    const suburb = form.$("suburb");
    const state = form.$("state");
    const postcode = form.$("postcode");
    const country = form.$("country");
    const type = null;
    const errors = form.errors();
    return (
      <React.Fragment>
        <form onSubmit={form.onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                {...address1.bind({ type })}
                variant="outlined"
                fullWidth
                autoComplete="address-line1"
                error={errors.address1 != null}
                helperText={errors.address1}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                {...address2.bind({ type })}
                variant="outlined"
                fullWidth
                autoComplete="address-line2"
                error={errors.address2 != null}
                helperText={errors.address2}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                {...suburb.bind({ type })}
                variant="outlined"
                fullWidth
                autoComplete="address-line2"
                error={errors.suburb != null}
                helperText={errors.suburb}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                select
                {...state.bind({ type })}
                variant="outlined"
                fullWidth
                autoComplete="state"
                error={errors.state != null}
                helperText={errors.state}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu
                  }
                }}
              >
                {controller.states.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                {...postcode.bind({ type })}
                fullWidth
                autoComplete="postal-code"
                error={errors.postcode != null}
                variant="outlined"
                helperText={errors.postcode}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                {...country.bind({ type })}
                value="Australia"
                variant="outlined"
                disabled
                fullWidth
                autoComplete="country"
              />
            </Grid>
          </Grid>
          <React.Fragment>
            <div className={classes.buttons}>
              <Button
                className={classes.button}
                onClick={() => controller.handleBack()}
              >
                Back
              </Button>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
              >
                Next
              </Button>
            </div>
          </React.Fragment>
        </form>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(LocationAddressView);
