import React, { Component } from "react";
import PlaceIcon from "@material-ui/icons/Place";

class MapMarker extends Component<Props> {
  render() {
    return <PlaceIcon fontSize="large" color="error" />;
  }
}
export default MapMarker;
