// @flow
import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { withStyles } from "@material-ui/core/styles";
import { observer } from "mobx-react";

type Props = {
  classes: any,
  title: string,
  description: string,
  eventDate: string,
  onEdit: () => mixed,
  onDelete: () => mixed
};

const styles = {
  card: {},
  description: {
    paddingBottom: 10
  }
};

@observer
class AnnouncementCard extends Component<Props> {
  render() {
    const { classes, title, description, eventDate } = this.props;
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Typography
            className={classes.description}
            variant="body2"
            color="textSecondary"
            component="p"
          >
            {description}
          </Typography>
          <Typography variant="body2" component="p">
            <b>Event Date - {eventDate}</b>
          </Typography>
        </CardContent>
        <CardActions>
          <IconButton onClick={this.props.onEdit} aria-label="Edit">
            <EditIcon />
          </IconButton>
          <IconButton onClick={this.props.onDelete} aria-label="Delete">
            <DeleteIcon />
          </IconButton>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles)(AnnouncementCard);
