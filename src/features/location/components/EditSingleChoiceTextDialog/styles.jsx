// @flow
const styles = () => ({
  serviceIcon: {
    marginRight: 10
  }
});
export default styles;
