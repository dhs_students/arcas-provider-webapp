// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  parentProps: any
};

@observer
class OpeningHoursView extends Component<Props> {
  render() {
    const { classes, controller, parentProps } = this.props;
    const { submitButtonTitle } = controller;
    return (
      <div className={classes.root}>
        {controller.days.map(day => this.renderDay(day))}
        {!parentProps.isDialog && (
          <React.Fragment>
            <div className={classes.buttons}>
              <Button
                className={classes.button}
                onClick={() => controller.handleCancel()}
              >
                Cancel
              </Button>
              <Button
                className={classes.button}
                onClick={() => controller.handleBack()}
              >
                Back
              </Button>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                onClick={() => controller.handleNext()}
                className={classes.button}
              >
                {submitButtonTitle}
              </Button>
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }

  renderDay = dayName => {
    const { classes, controller } = this.props;
    const {
      handleOpenSwitchChange,
      updateTime,
      deleteTime,
      addTime
    } = controller;
    const dayObj = controller.openingHours[dayName];
    return dayObj.times.map((time, index) => (
      <div key={`${index + 1}`}>
        <Grid container spacing={2}>
          <Grid item xs={8} sm={2} md={2}>
            {index === 0 && (
              <Typography className={classes.dayName}>{dayName}</Typography>
            )}
          </Grid>
          <Grid item xs={4} sm={2} md={2}>
            {index === 0 && (
              <FormControlLabel
                control={
                  <Switch
                    checked={dayObj.isOpen}
                    onChange={event => handleOpenSwitchChange(event, dayName)}
                    color="primary"
                    value={dayName}
                  />
                }
                label={dayObj.isOpen ? "Open" : "Closed"}
              />
            )}
          </Grid>
          <Grid item xs={5} sm={2} md={2}>
            {dayObj.isOpen && (
              <TextField
                id="time"
                label="Start Time"
                type="time"
                defaultValue={time.startTime}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
                onChange={event =>
                  updateTime(event, "startTime", dayName, index)
                }
              />
            )}
          </Grid>
          <Grid item xs={5} sm={2} md={2}>
            {dayObj.isOpen && (
              <TextField
                id="time"
                label="End Time"
                type="time"
                error={time.hasError}
                helperText={
                  time.hasError && "End time should be greater then start time."
                }
                defaultValue={time.endTime}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
                onChange={event => updateTime(event, "endTime", dayName, index)}
              />
            )}
          </Grid>
          <Grid item xs={2} sm={2} md={2}>
            {dayObj.isOpen && index > 0 && (
              <IconButton
                aria-label="Delete"
                onClick={() => deleteTime(dayName, index)}
              >
                <DeleteIcon />
              </IconButton>
            )}
          </Grid>
          <Grid item xs={12} sm={2} md={2}>
            {dayObj.isOpen && dayObj.times.length === 1 && (
              <Button
                color="secondary"
                className={classes.addButton}
                onClick={() => addTime(dayName)}
              >
                Add Hours
              </Button>
            )}
          </Grid>
        </Grid>
        <br />
      </div>
    ));
  };
}

export default withStyles(styles)(OpeningHoursView);
