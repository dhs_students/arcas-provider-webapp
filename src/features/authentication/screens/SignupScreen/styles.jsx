// @flow
import commonStyles from "features/authentication/styles/authStyles";

const styles = (theme: any) => ({
  ...commonStyles(theme),
  itemSameLine: {
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 20
    }
  },
  submitWrapper: {
    paddingBottom: 30
  },
  formGroupContainer: {
    paddingTop: 10
  }
});

export default styles;
