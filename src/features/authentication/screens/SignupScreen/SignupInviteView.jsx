// @flow
import React, { Component } from "react";
import { computed } from "mobx";
import { observer } from "mobx-react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContentWrapper from "common/components/SnackBarContentWrapper";
import AuthLayout from "common/layouts/AuthLayout";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any,
  invitedfname: string,
  invitedlname: string,
  invitedemail: string
};

@observer
class SignupInviteView extends Component<Props> {
  constructor(props: Object) {
    super(props);
    this.props.form.$("firstName").value = this.props.invitedfname;
    this.props.form.$("lastName").value = this.props.invitedlname;
  }

  render() {
    const { classes, controller, form, invitedemail } = this.props;
    const {
      handleSnackbarClose,
      signUpFailed,
      errorMessage,
      isSigningUp
    } = controller;
    const email = form.$("email");
    email.value = invitedemail;
    const password = form.$("password");
    const confirmPassword = form.$("passwordConfirm");
    const firstName = form.$("firstName");
    const lastName = form.$("lastName");
    const type = null;
    const errors = form.errors();

    return (
      <AuthLayout>
        <CssBaseline />
        <Grid container>
          <div className={classes.formHeaderContainer}>
            <Typography className={classes.formHeaderTitle} variant="headline">
              Confirm Registration
            </Typography>
            <Typography variant="subtitle1">
              Complete signing up by invitation
            </Typography>
          </div>
          <form className={classes.formContainer} onSubmit={form.onSubmit}>
            <Grid container>
              <Grid item xs={12}>
                <TextField
                  {...email.bind({ type })}
                  margin="normal"
                  disabled
                  variant="outlined"
                  helperText={errors.email}
                  error={errors.email != null}
                  value={invitedemail}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...firstName.bind({ type })}
                  margin="normal"
                  variant="outlined"
                  helperText={errors.firstName}
                  error={errors.firstName != null}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...lastName.bind({ type })}
                  margin="normal"
                  variant="outlined"
                  helperText={errors.lastName}
                  error={errors.lastName != null}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...password.bind({ type })}
                  autoComplete="password"
                  margin="normal"
                  variant="outlined"
                  helperText={
                    errors.password
                      ? errors.password
                      : "Your password must be between 5 and 25 characters"
                  }
                  error={errors.password != null}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...confirmPassword.bind({ type })}
                  margin="normal"
                  variant="outlined"
                  helperText={errors.passwordConfirm}
                  error={errors.passwordConfirm != null}
                  fullWidth
                />
              </Grid>
            </Grid>
            <div className="submitWrapper">
              <div className={classes.buttonWrapper}>
                {isSigningUp && (
                  <CircularProgress
                    size={22}
                    className={classes.buttonProgress}
                  />
                )}
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  disabled={isSigningUp}
                  color="primary"
                  className={classes.submitButton}
                >
                  Sign up
                </Button>
              </div>
            </div>
          </form>

          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left"
            }}
            open={signUpFailed}
            autoHideDuration={6000}
            onClose={handleSnackbarClose}
          >
            <SnackbarContentWrapper
              onClose={handleSnackbarClose}
              variant="error"
              message={errorMessage}
            />
          </Snackbar>
        </Grid>
      </AuthLayout>
    );
  }

  @computed
  get passwordHelperText(): string {
    const { form } = this.props;
    const errors = form.errors();
    if (errors.password != null) {
      return errors.password;
    }
    return "Minimum 5 characters long";
  }
}

export default withStyles(styles)(SignupInviteView);
