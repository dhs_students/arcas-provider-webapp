// @flow
import React, { Component } from "react";
import { computed } from "mobx";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles from "@material-ui/core/styles/withStyles";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContentWrapper from "common/components/SnackBarContentWrapper";
import AuthLayout from "common/layouts/AuthLayout";
import {
  FadeInAnimation,
  FadeInLeftAnimation,
  FadeInRightAnimation
} from "common/animations";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any
};

@observer
class SignupView extends Component<Props> {
  render() {
    const { classes, controller, form } = this.props;
    const {
      handleSnackbarClose,
      signUpFailed,
      errorMessage,
      isSigningUp
    } = controller;
    const orgName = form.$("name");
    const email = form.$("email");
    const password = form.$("password");
    const confirmPassword = form.$("passwordConfirm");
    const phone = form.$("phone");
    const abn = form.$("abn");
    const firstName = form.$("firstName");
    const lastName = form.$("lastName");
    const type = null;
    const errors = form.errors();
    return (
      <AuthLayout>
        <CssBaseline />
        <Grid container>
          <FadeInAnimation>
            <div className={classes.formHeaderContainer}>
              <Typography
                className={classes.formHeaderTitle}
                variant="h6"
                component="h1"
              >
                Sign up
              </Typography>
              <Typography variant="subtitle1">
                Complete the form to become a member
              </Typography>
            </div>
          </FadeInAnimation>
          <form className={classes.formContainer} onSubmit={form.onSubmit}>
            {/* Contact Details */}
            <Grid container className={classes.formGroupContainer}>
              <Grid item xs={12}>
                <Typography className={classes.bold} variant="subtitle1">
                  Contact person details
                </Typography>
              </Grid>
              <Grid container>
                <Grid item xs={12}>
                  <FadeInLeftAnimation>
                    <TextField
                      {...firstName.bind({ type })}
                      autoComplete="fname"
                      margin="normal"
                      variant="outlined"
                      helperText={errors.firstName}
                      error={errors.firstName != null}
                      fullWidth
                      autoFocus
                    />
                  </FadeInLeftAnimation>
                </Grid>
                <Grid item xs={12}>
                  <FadeInRightAnimation>
                    <TextField
                      {...lastName.bind({ type })}
                      autoComplete="lname"
                      margin="normal"
                      variant="outlined"
                      helperText={errors.lastName}
                      error={errors.lastName != null}
                      fullWidth
                    />
                  </FadeInRightAnimation>
                </Grid>

                <Grid item xs={12}>
                  <FadeInLeftAnimation>
                    <TextField
                      {...email.bind({ type })}
                      autoComplete="email"
                      margin="normal"
                      variant="outlined"
                      helperText={errors.email}
                      error={errors.email != null}
                      fullWidth
                    />
                  </FadeInLeftAnimation>
                </Grid>
              </Grid>
            </Grid>
            {/* Password Details */}
            <Grid container className={classes.formGroupContainer}>
              <FadeInAnimation>
                <Grid className={classes.subTitle}>
                  <Typography className={classes.bold} variant="subtitle2">
                    Enter a strong password
                  </Typography>
                  <Typography variant="body1">
                    Must be between 5 and 25 characters
                  </Typography>
                </Grid>
              </FadeInAnimation>
              <Grid container>
                <Grid item xs={12}>
                  <FadeInLeftAnimation>
                    <TextField
                      {...password.bind({ type })}
                      autoComplete="password"
                      margin="normal"
                      variant="outlined"
                      helperText={errors.password}
                      error={errors.password != null}
                      fullWidth
                    />
                  </FadeInLeftAnimation>
                </Grid>

                <Grid item xs={12}>
                  <FadeInRightAnimation>
                    <TextField
                      {...confirmPassword.bind({ type })}
                      margin="normal"
                      variant="outlined"
                      helperText={errors.passwordConfirm}
                      error={errors.passwordConfirm != null}
                      fullWidth
                    />
                  </FadeInRightAnimation>
                </Grid>
              </Grid>
            </Grid>
            {/* Organisation Details */}
            <Grid container className={classes.formGroupContainer}>
              <FadeInAnimation>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" className={classes.bold}>
                    Organisation details
                  </Typography>
                </Grid>
              </FadeInAnimation>
              <Grid item xs={12}>
                <FadeInLeftAnimation>
                  <TextField
                    {...orgName.bind({ type })}
                    autoComplete="orgName"
                    margin="normal"
                    variant="outlined"
                    helperText={errors.name}
                    error={errors.name != null}
                    fullWidth
                  />
                </FadeInLeftAnimation>
              </Grid>
              <Grid container>
                <Grid item xs={12}>
                  <FadeInRightAnimation>
                    <TextField
                      {...phone.bind({ type })}
                      margin="normal"
                      variant="outlined"
                      helperText={errors.phone}
                      error={errors.phone != null}
                      fullWidth
                    />
                  </FadeInRightAnimation>
                </Grid>
                <Grid item xs={12}>
                  <FadeInLeftAnimation>
                    <TextField
                      {...abn.bind({ type })}
                      margin="normal"
                      variant="outlined"
                      helperText={errors.abn}
                      error={errors.abn != null}
                      fullWidth
                    />
                  </FadeInLeftAnimation>
                </Grid>
              </Grid>
            </Grid>
            <div className={classes.submitWrapper}>
              <div className={classes.buttonWrapper}>
                {isSigningUp && (
                  <CircularProgress
                    size={22}
                    className={classes.buttonProgress}
                  />
                )}
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  disabled={isSigningUp}
                  color="primary"
                  className={classes.submitButton}
                >
                  Sign up
                </Button>
              </div>
              <Divider className={classes.divider} />
              <Typography
                variant="caption"
                className={`${classes.textLinkDark} ${classes.centerText}`}
              >
                <Link to="/login" className={classes.textLinkDark}>
                  Return to login
                </Link>
              </Typography>
            </div>
          </form>
        </Grid>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={signUpFailed}
          autoHideDuration={6000}
          onClose={handleSnackbarClose}
        >
          <SnackbarContentWrapper
            onClose={handleSnackbarClose}
            variant="error"
            message={errorMessage}
          />
        </Snackbar>
      </AuthLayout>
    );
  }

  @computed
  get passwordHelperText(): string {
    const { form } = this.props;
    const errors = form.errors();
    if (errors.password != null) {
      return errors.password;
    }
    return "Minimum 5 characters long";
  }
}

export default withStyles(styles)(SignupView);
