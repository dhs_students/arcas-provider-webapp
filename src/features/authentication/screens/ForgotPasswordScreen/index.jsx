// @flow
import React, { Component } from "react";
import { action, computed } from "mobx";
import { inject, observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import history from "appHistory";
import ForgotPasswordView from "./ForgotPasswordView";

type Props = {
  AuthStore: any,
  UiEventStore: any
};

@inject("AuthStore", "UiEventStore")
@observer
class ForgotPasswordScreen extends Component<Props> {
  render() {
    return (
      <ForgotPasswordView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: any) => {
    const { email } = form.values();
    const { AuthStore, UiEventStore } = this.props;
    AuthStore.forgotPassword(email)
      .then(() => {
        UiEventStore.triggerSnackbarSuccess(
          "Email sent. Please follow instructions."
        );
        history.push("/login");
      })
      .catch(() => {
        UiEventStore.triggerSnackbarFail(
          "Failed to reset password for the email."
        );
      });
  };

  @action
  onError = () => {};

  @computed
  get isSendingEmail(): boolean {
    return this.props.AuthStore.isLoading;
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "email",
        type: "email",
        label: "Email",
        rules: "required|email|string|max:100"
      }
    ];
  }
}

export default ForgotPasswordScreen;
