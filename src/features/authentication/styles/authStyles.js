// @flow
const formInputAdornments = {
  inputAdornmentContainer: {
    paddingTop: 5
  },
  inputAdornmentLabel: {
    paddingLeft: 6,
    paddingTop: 1
  }
};

const formHeaderComponents = (theme: any) => ({
  formHeaderContainer: {
    paddingBottom: 10,
    paddingLeft: 10,
    [theme.breakpoints.up("md")]: {
      paddingTop: 80,
      paddingLeft: 60
    },
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 60
    },
    [theme.breakpoints.down("xs")]: {
      paddingTop: 10,
      paddingLeft: 10
    }
  },
  formHeaderTitle: {
    fontWeight: "bold"
  }
});

const styles = (theme: any) => ({
  ...formInputAdornments,
  ...formHeaderComponents(theme),
  formContainer: {
    width: "100%", // Fix IE11 issue.
    marginTop: theme.spacing(1),
    paddingLeft: 10,
    paddingRight: 10,
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 60,
      paddingRight: 60
    }
  },
  bold: {
    fontWeight: "bold"
  },
  centerText: {
    textAlign: "center"
  },
  rightAlignText: {
    textAlign: "right"
  },
  submitButton: {
    marginTop: 15,
    padding: 15
  },
  textLinkDark: {
    marginTop: 10,
    textDecoration: "none",
    color: "#757575",
    "&:focus, &:visited, &:link, &:active": {
      textDecoration: "none"
    },
    "&:hover": {
      textDecoration: "none",
      color: "#2196f3"
    }
  },
  divider: {
    color: "#333",
    marginTop: 30
  },
  textLinkPrimary: {
    textDecoration: "none",
    color: theme.palette.primary.light,
    "&:focus, &:visited, &:link, &:active": {
      textDecoration: "none"
    },
    "&:hover": {
      textDecoration: "none",
      color: "#2196f3"
    }
  },
  buttonProgress: {
    color: "#fff",
    position: "absolute",
    top: "40%",
    left: "70%",
    zindex: 1
  },
  buttonWrapper: {
    position: "relative"
  },
  centerTextTopPadding: {
    marginTop: 20,
    textAlign: "center"
  }
});

export default styles;
