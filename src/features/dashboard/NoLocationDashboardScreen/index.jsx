// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { computed } from "mobx";
import moment from "moment";
import NoLocationDashboardView from "./NoLocationDashboardView";

type Props = {
  UserStore: any,
  SupplierOfferStore: any
};

const OFFER_COUNT = 3;

@inject(
  "AuthStore",
  "UserStore",
  "SupplierOfferStore",
  "LocationStore",
  "RefStore"
)
@observer
class NoLocationDashboardScreen extends Component<Props> {
  componentDidMount() {
    const { SupplierOfferStore, UserStore } = this.props;
    SupplierOfferStore.getSubsetMyOffers(
      UserStore.currentOrganisation.orgName,
      OFFER_COUNT
    );
    SupplierOfferStore.getSubsetAllOffers(OFFER_COUNT);
  }

  @computed
  get isSupplierOffersLoaded(): boolean {
    const { SupplierOfferStore } = this.props;
    return SupplierOfferStore.myData != null;
  }

  @computed
  get isAllOffersLoaded(): boolean {
    const { SupplierOfferStore } = this.props;
    return SupplierOfferStore.allData != null;
  }

  @computed
  get supplierObjects(): Array<Object> {
    const { SupplierOfferStore } = this.props;
    if (SupplierOfferStore.myData) {
      return SupplierOfferStore.myData.map(x => {
        let image;
        if (x.images && x.images.length > 0) {
          image = x.images[0]; // eslint-disable-line
        }
        return {
          id: x.id,
          title: x.title,
          description: x.description,
          location: x.location,
          avatar: x.orgAvatar,
          subtitle: x.orgName,
          image,
          endDate: moment(x.endDate.toDate()).format("DD MMMM YYYY")
        };
      });
    }
    return [];
  }

  @computed
  get latestOffersObjects(): Array<Object> {
    const { SupplierOfferStore } = this.props;
    if (SupplierOfferStore.allData) {
      return SupplierOfferStore.allData.map(x => {
        let image;
        if (x.images && x.images.length > 0) {
          image = x.images[0]; // eslint-disable-line
        }
        return {
          id: x.id,
          title: x.title,
          description: x.description,
          location: x.location,
          avatar: x.orgAvatar,
          subtitle: x.orgName,
          image,
          endDate: moment(x.endDate.toDate()).format("DD MMMM YYYY")
        };
      });
    }
    return [];
  }

  render() {
    if (this.props.UserStore) {
      return <NoLocationDashboardView controller={this} />;
    }
    return <div />;
  }
}

export default NoLocationDashboardScreen;
